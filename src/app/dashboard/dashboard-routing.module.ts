import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GeneradorCodigosComponent } from './pages/generador-codigos/generador-codigos.component';
import { PanelComponent } from './pages/panel/panel.component';
import { PrincipalDashboardComponent } from './pages/principal-dashboard/principal-dashboard.component';

const routes: Routes = [
  
  {
    path: '',
    component: PrincipalDashboardComponent,
    children: [
      {
        path:'panel',
        component:PanelComponent,
      },
      {
        path:'generador',
        component:GeneradorCodigosComponent,
      },
      {
        path:'**',
        redirectTo:'panel',
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
