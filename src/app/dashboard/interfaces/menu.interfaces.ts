export interface MenuItem{
    titulo: string;
    ruta: string;
    icono: string;
}