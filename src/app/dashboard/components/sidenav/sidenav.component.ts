import { Component, OnInit } from '@angular/core';
import { MenuItem } from '../../interfaces/menu.interfaces';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styles: [
  ]
})
export class SidenavComponent implements OnInit {

  menuitems: MenuItem[] = [
    {
      titulo:'Dashboard',
      ruta:'dashboard/panel',
      icono:'dashboard'
    },
    {
      titulo:"Generador de codigos",
      ruta:'/dashboard/generador',
      icono:'pin'
    }
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
