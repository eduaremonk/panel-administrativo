import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { PrincipalDashboardComponent } from './pages/principal-dashboard/principal-dashboard.component';
import { PanelComponent } from './pages/panel/panel.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { MaterialModule } from '../material/material.module';
import { GeneradorCodigosComponent } from './pages/generador-codigos/generador-codigos.component';


@NgModule({
  declarations: [
    PrincipalDashboardComponent,
    PanelComponent,
    SidenavComponent,
    GeneradorCodigosComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MaterialModule
  ]
})
export class DashboardModule { }
