import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm : FormGroup = this.fb.group({
    email:['test@emonkonline.com',[Validators.required, Validators.email]],
    password:['123456',[Validators.required, Validators.minLength(6)]],
  });
  constructor(private fb: FormBuilder, private router: Router) { }

  ngOnInit(): void {
  }


  login() {
    
    const { email, password } = this.loginForm.value;
    this.router.navigateByUrl('/dashboard');

  }

}
